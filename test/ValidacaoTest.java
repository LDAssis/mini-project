
import apoio.Formatacao;
import apoio.Validacao;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Fritzzin
 */
public class ValidacaoTest {

    public ValidacaoTest() {

    }

    // Augusto Fritz
    // Fazer um com pontuacao e outro sem?
    @Test
    public void testeCPF() {
//        String cpf = "033.711.300-96";
        String cpf = "03371130096";
        assertEquals(true, Validacao.validarCPF(cpf));
    }
    
    // Augusto Fritz
    @Test
    public void testeCNPJ() {
        String cnpj = "70.868.448/0001-07";
//        String cnpj = "70868448000107";
        assertEquals(true, Validacao.validarCNPJ(cnpj));
    }
    @Test
    public void testeTemNumero(){
        String numeros = "123456789";
        assertEquals(true,Validacao.temNumeros(numeros));
    }
    @Test
    public void testeLetras(){
        String letras = "testeNervoso1";
        assertEquals(true,Validacao.temLetras(letras));
    }
    
    //Lucas
    @Test
    public void testeValidarDataDMACerto(){
        int d = 10;
        int m = 9;
        int a = 2020;
        assertEquals(true, Validacao.validarDataDMA(d, m, a));
    }
    
    //Lucas
    @Test
    public void testeValidarDataDMAErrado(){
        int d = 33;
        int m = 20;
        int a = 2020;
        assertEquals(false, Validacao.validarDataDMA(d, m, a));
    }
    
    //Lucas
    @Test
    public void testeValidarDataFormatadaCerto(){
        String data = "19/9/1999";
        assertEquals(true, Validacao.validarDataFormatada(data));
    }
    
    //Lucas
    @Test
    public void testeValidarDataFormatadaErrado(){
        String data = "33/1/2020";
        assertEquals(false, Validacao.validarDataFormatada(data));
    }
    
    //Lucas
    @Test
    public void testegetDataAtual(){
        Date now = new Date();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String dataHoje = df.format(now);
        assertEquals(dataHoje, Formatacao.getDataAtual());
    }
    
    @Test
    public void testeVazioCerto(){
        String s = "";
        assertEquals(true, Validacao.vazio(s));
    }
    
    @Test
    public void testeVazioErrado(){
        String s = "asdasid jaskdl jaslkd ";
        assertEquals(false, Validacao.vazio(s));
    }
    
    
    
}
